module.exports = {
  publicPath: process.env.BASE_URL,
  devServer: {
    port: 8100
  }
}